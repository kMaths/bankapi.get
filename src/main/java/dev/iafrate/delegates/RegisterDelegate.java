package dev.iafrate.delegates;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import dev.iafrate.beans.User;
import dev.iafrate.data.AccountPostgres;
import dev.iafrate.data.RolePostgres;
import dev.iafrate.data.UserPostgres;
import dev.iafrate.services.UserService;

public class RegisterDelegate implements FrontControllerDelegate {
	private UserService us = new UserService(new UserPostgres(), new RolePostgres(), new AccountPostgres());
	private ObjectMapper om = new ObjectMapper();
	
	@Override
	public void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		User user = (User) om.readValue(req.getInputStream(), User.class);
		User thisUser = (User) req.getSession().getAttribute("user");
		
		if("admin".equals(thisUser.getRole().getRole())) {
			if(user.getUserId()==0 && isNotNull(user) ) {
				
				user.setUserId(us.registerUser(user));
				resp.setStatus(201);
				resp.getWriter().write(om.writeValueAsString(user));
				
			} else {
				resp.sendError(401, "Invalid fields");
			}
		} else {
			resp.sendError(401, "The requested action is not permitted");
		}

	}
	
	private boolean isNotNull  (User user) {
		
		if (user.getEmail()==null || user.getFirstName()==null||user.getLastName()==null||user.getPassword()==null
				||user.getRole()==null||user.getUsername()==null) {
			return false;
		}
		return true;
	}

}
