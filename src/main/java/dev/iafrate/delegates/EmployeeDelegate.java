package dev.iafrate.delegates;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import dev.iafrate.beans.Account;
import dev.iafrate.beans.User;
import dev.iafrate.data.AccountPostgres;
import dev.iafrate.data.AccountStatusPostgres;
import dev.iafrate.data.RolePostgres;
import dev.iafrate.data.UserPostgres;
import dev.iafrate.services.AccountService;
import dev.iafrate.services.UserService;

/*
 *  Endpoints:
 *  	/users - (GET) retrieves all users
 *  		     (PUT) updates user (self)
 *  	
 *  	/users/:id/account = (POST) submits an account
 *  	
 *  	/users/:id - (GET) retrieves a user by id
 *  
 */

public class EmployeeDelegate implements FrontControllerDelegate {
	ObjectMapper om = new ObjectMapper();
	UserService userService = new UserService(new UserPostgres(), new RolePostgres(), new AccountPostgres());
	AccountService accountService = new AccountService(new AccountPostgres(), new UserPostgres(), new AccountStatusPostgres());
	
	@Override
	public void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String path = (String) req.getAttribute("path");
		
		if(path == "" || path == null) {
			switch (req.getMethod()) {
			case "GET":
				resp.getWriter().write(om.writeValueAsString(userService.getAllUsers()));
				break;
			case "PUT":
				updateUser(req,resp);
				break;
			default:
				resp.sendError(401, "The requested action is not permitted");
			}
			
		} else if(path.contains("account")){
			if ("POST".equals(req.getMethod())) {
				createAccount(req, resp, path);
			} else {
				resp.sendError(401, "The requested action is not permitted");
			}
		} else {
			if("GET".equals(req.getMethod())) {
				getUserById(req, resp, path);
			}
		}

	}
	public void updateUser(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		User user = (User) om.readValue(req.getInputStream(), User.class);
		String role = user.getRole().getRole();
		User thisUser = (User) req.getSession().getAttribute("user");
		if(isNotNull(user) && thisUser.getUserId()==user.getUserId() && "standard".equals(role) && userService.updateUser(user) != null) {
			resp.getWriter().write(om.writeValueAsString(user));
		} else {
			resp.sendError(401, "The requested action is not permitted");
		}
	}
	
	private void getUserById(HttpServletRequest req, HttpServletResponse resp, String path) throws JsonProcessingException, IOException {
		User user = userService.getUserById(Integer.parseInt(path));
		if(user!=null) {
			resp.getWriter().write(om.writeValueAsString(user));
		} else {
			resp.sendError(401,"The requested action is not permitted");
		}	
	}
	
	private void createAccount(HttpServletRequest req, HttpServletResponse resp, String path) throws IOException{
		StringBuilder path1 = new StringBuilder(path);
		Integer id = Integer.valueOf(path1.replace(path1.indexOf("/"), path1.length(), "").toString());
		Account account = (Account) om.readValue(req.getInputStream(), Account.class);
		User user = userService.getUserById(id);
		
		if(isNotNull(account) && isNotNull(user)&& account.getAccountId()==0 ) {
			Account newAccount = accountService.submitAccount(account, user);
			if(newAccount!=null && newAccount.getAccountId()!=0) {
				resp.getWriter().write(om.writeValueAsString(newAccount));
			} else {
				resp.sendError(401, "The requested action is not permitted");
			}
		} else {
			resp.sendError(401, "The requested action is not permitted");
		}
	}
	
	private boolean isNotNull  (User user) {
		
		if (user.getEmail()==null || user.getFirstName()==null||user.getLastName()==null||user.getPassword()==null
				||user.getRole()==null||user.getUsername()==null) {
			return false;
		}
		return true;
	}
	private boolean isNotNull  (Account account) {
		
		if (account.getStatus()==null||account.getType()==null) {
			return false;
		}
		return true;
	}
	

}
