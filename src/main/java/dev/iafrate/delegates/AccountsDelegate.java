package dev.iafrate.delegates;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.iafrate.beans.Account;
import dev.iafrate.beans.User;
import dev.iafrate.data.AccountPostgres;
import dev.iafrate.data.AccountStatusPostgres;
import dev.iafrate.data.RolePostgres;
import dev.iafrate.data.UserPostgres;
import dev.iafrate.services.AccountService;
import dev.iafrate.services.UserService;
/*/:
 *  /accounts
 * 		PUT: Update account 
 * 		GET: retrieve all accounts
 * 
 *  /accounts/:id
 * 		GET: Find account by account id
 * 
 *  /accounts/transfer
 *  	POST: Transfer money from source account to target account
 *  
 *  /accounts/withdraw
 *  	POST: Withdraw money
 *  
 *  /accounts/deposit
 *  	POST: Deposit money
 *  
 *  /accounts/status/:statusid
 *  	GET: get accounts by status
 *  
 *  /accounts/user/:id
 *  	GET: get accounts by user id
 */

	

public class AccountsDelegate implements FrontControllerDelegate {
	AccountService accountService = new AccountService (new AccountPostgres(), new UserPostgres(), new AccountStatusPostgres());
	UserService userService = new UserService(new UserPostgres(), new RolePostgres(), new AccountPostgres());
	ObjectMapper om = new ObjectMapper();
	
	
	@Override
	public void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//TODO change to switch
		String path = (String) req.getAttribute("path");
		
		if(path == "" || path == null) {
			switch(req.getMethod()) {
			case "GET":
				getAllAccounts(req, resp);
				break;
			case "PUT":
				updateAccount(req, resp);
				break;
			default:
				resp.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
			}
			
		} else if("deposit".equals(path)) {
			if("POST".equals(req.getMethod())) {
				deposit(req, resp);
			} else {
				resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
			}
			
		} else if ("withdraw".equals(path)) {
			if("POST".equals(req.getMethod())) {
				withdraw(req, resp);
			} else {
				resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
			}
			
		} else if ("transfer".equals(path)) {
			if("POST".equals(req.getMethod())) {
				transfer(req, resp);
			} else {
				resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
			}
			
		} else if(path.contains("status")){
			getAccountsByStatus(req, resp, path);
			
		} else if(path.contains("owner")){
			if("GET".equals(req.getMethod())) {
				getAccountsByOwner(req,resp,path);
			} else {
				resp.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
			}
		} else {
			if("GET".equals(req.getMethod())) {
				getAccountById(req, resp, path);
			} else if("DELETE".equals(req.getMethod())){
				deleteAccount(req, resp, path);
			} else {
				resp.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
			}				
		}
	}


	


	private void getAccountsByStatus(HttpServletRequest req, HttpServletResponse resp, String path) throws IOException {
		StringBuilder path1 = new StringBuilder(path);
		Integer id = Integer.valueOf(path1.substring(path1.indexOf("/")+1));
		Set<Account> accounts = accountService.getAccountsByStatus(id);
		User user = (User) req.getSession().getAttribute("user");
		String role = user.getRole().getRole();
		if("admin".equals(role) || "employee".equals(role)) {
			resp.getWriter().write(om.writeValueAsString(accounts));
		} else {
			resp.sendError(401, "The requested action is not permitted");
		}
		
		
	}
	public void getAllAccounts(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		User user = (User) req.getSession().getAttribute("user");
		
		String role = user.getRole().getRole();
		//ensure that user is admin or employee
		if((role.equals("admin")||role.equals("employee"))){
			Set<Account> accounts = accountService.getAllAccounts();
			//if accounts is null, something went wrong
			if(accounts != null) {
				resp.getWriter().write(om.writeValueAsString(accounts));
				resp.setStatus(HttpServletResponse.SC_OK);
			} else {
			resp.sendError(500);
			}
		} else {
			resp.sendError(401, "The requested action is not permitted");
		}
		
	}
	
	public void updateAccount(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		User user = (User) req.getSession().getAttribute("user");
		Account updateAccount = om.readValue(req.getInputStream(), Account.class);
		if (isNotNull(updateAccount)){
			updateAccount = accountService.updateAccount(updateAccount, user);
			if(updateAccount != null) {
				if(updateAccount.getAccountId()!=0) {
					resp.getWriter().write(om.writeValueAsString(updateAccount));
				} else {
					resp.sendError(401,"The requested action is not permitted");
				}
			}else {
				resp.sendError(401,"The requested action is not permitted");
			}
			
		} else {
			resp.sendError(401,"The requested action is not permitted");
		}
	
	}
	public void getAccountById(HttpServletRequest req, HttpServletResponse resp, String path) throws IOException{
		int id = Integer.valueOf(path);
		User user = (User) req.getSession().getAttribute("user");
		Account account = accountService.getAccountById(id, user);
		if(account != null) {
			if(account.getAccountId() > 0) {
				resp.getWriter().write(om.writeValueAsString(account));
				resp.setStatus(HttpServletResponse.SC_OK);
			} else {
				resp.sendError(500);
			}
		} else {
			resp.sendError(401,"The requested action is not permitted");
		}
	}
	private void deleteAccount(HttpServletRequest req, HttpServletResponse resp, String path) throws IOException {
		Integer id = Integer.valueOf(path);
		if(accountService.deleteAccount(id)) {
			resp.setStatus(HttpServletResponse.SC_OK);
		} else {
			resp.sendError(401, "The requested action is not permitted");
		}
		
	}
	private void getAccountsByOwner(HttpServletRequest req, HttpServletResponse resp, String path) throws JsonProcessingException, IOException {
		StringBuilder path1 = new StringBuilder(path);
		User user = (User) req.getSession().getAttribute("user");
		Integer id = Integer.valueOf(path1.substring(path1.indexOf("/")+1));
		Set<Account> accounts = accountService.getAccountsByUser(id, user);
		if(accounts != null && !accounts.isEmpty()) {
			resp.getWriter().write(om.writeValueAsString(accounts));
		} else {
			resp.sendError(401, "The requested action is not permitted");
		}
		
	}
	
	private void deposit(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		@SuppressWarnings("unchecked")
		Map<String, Object> jsonMap = om.readValue(req.getInputStream(), Map.class);
		Integer id = (Integer) jsonMap.get("accountId");
		Double amount = (Double) jsonMap.get("amount");
		User user = (User) req.getSession().getAttribute("user");
		Account account = accountService.getAccountById(id, user);
		
		if(account!=null) {
			account.setBalance(account.getBalance()+amount);
			if(accountService.updateAccount(account, user) !=null) {
				account = accountService.updateAccount(account, user);
				resp.getWriter().write(amount + " has been deposited Account " + id);
			} else {
				resp.sendError(401, "the requested action is not permitted");
			}
		} else {
			resp.sendError(401, "the requested action is not permitted");
		} 
		
		
	}
	private void withdraw(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		@SuppressWarnings("unchecked")
		Map<String, Object> jsonMap = om.readValue(req.getInputStream(), Map.class);
		Integer id = (Integer) jsonMap.get("accountId");
		Double amount = (Double) jsonMap.get("amount");
		User user = (User) req.getSession().getAttribute("user");
		Account account = accountService.getAccountById(id, user);
		
		
		if(account!=null) {
			account.setBalance(account.getBalance()-amount);
			if(accountService.updateAccount(account, user) !=null) {
				resp.getWriter().write(amount + " has been withdrawn from Account " + id);
			} else {
				resp.sendError(401, "the requested action is not permitted");
			}
		} else {
			resp.sendError(401, "the requested action is not permitted");
		}
		
		
	}
	private void transfer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		@SuppressWarnings("unchecked")
		Map<String, Object> jsonMap = om.readValue(req.getInputStream(), Map.class);
		Integer sourceId = (Integer) jsonMap.get("sourceId");
		Integer targetId = (Integer) jsonMap.get("targetId");
		Double amount = (Double) jsonMap.get("amount");
		User user = (User) req.getSession().getAttribute("user");
		
		if(accountService.transferFunds(user, sourceId, targetId, amount)) {
			resp.getWriter().write(amount + " has been transferred from Account " + sourceId + " to Account " + targetId);
		} else {
			resp.sendError(401, "The requested action is not permitted");
		}
		
		
	}
	

	private boolean isNotNull  (Account account) {
		
		if (account.getStatus()==null||account.getType()==null) {
			return false;
		}
		return true;
	}


	
	
}




