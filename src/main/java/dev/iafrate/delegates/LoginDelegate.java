package dev.iafrate.delegates;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

import dev.iafrate.beans.User;
import dev.iafrate.data.AccountPostgres;
import dev.iafrate.data.RolePostgres;
import dev.iafrate.data.UserPostgres;
import dev.iafrate.services.UserService;
/*POST:
 * 	
 * 
 */
public class LoginDelegate implements FrontControllerDelegate {
	private ObjectMapper om = new ObjectMapper();  
	
	
	@Override
	public void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserService us = new UserService(new UserPostgres(), new RolePostgres(), new AccountPostgres());
		String path = (String) req.getAttribute("path");
		if (path == "" || path == null){
			
			if(req.getMethod().equals("POST")) {
				String username = req.getParameter("username");
				String password = req.getParameter("password");
				User u = us.getUserByUsername(username);
				User thisUser = (User) req.getSession().getAttribute("user");
				if(thisUser!=null) {
					req.getSession().invalidate();
				}
				if(u != null && u.getPassword().equals(password)) {
					req.getSession().setAttribute("user", u);
					resp.getWriter().write(om.writeValueAsString(u));
		
					} else {
					resp.sendError(401,"The requested action is not permitted");
				}
				
					
				
			} else {
				resp.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
			}
		} else {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		
		
	}

}
