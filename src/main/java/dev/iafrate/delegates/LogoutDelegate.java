package dev.iafrate.delegates;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dev.iafrate.beans.User;

public class LogoutDelegate implements FrontControllerDelegate {

	@Override
	public void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		if ("DELETE".equals(req.getMethod())) {
			if(req.getSession().getAttribute("user") != null) {
				resp.getWriter().write("You have successfully logged out " + ((User) req.getSession().getAttribute("user")).getUsername());
				req.getSession().invalidate();
			} else {
				resp.sendError(400, "There was no user logged into the session");
			}
			
		}else {
			resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);}

	}

}
