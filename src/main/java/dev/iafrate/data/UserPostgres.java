package dev.iafrate.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import dev.iafrate.beans.Role;
import dev.iafrate.beans.User;
import dev.iafrate.util.ConnectionUtil;

public class UserPostgres implements UserDAO {
	ConnectionUtil cu = ConnectionUtil.getConnectionUtil();

	@Override
	public Set<User> getAllUsers() {
		Set<User> users = new HashSet<>();
		
		try(Connection conn = cu.getConnection()){
			String sql = "select user_id, user_name, first_name, last_name, email, role.role_id, role.role_name from "
					+ "bank_user join role on bank_user.role_id = role.role_id";
			
			//create Statment does not take a parameter
			Statement stmt = conn.createStatement();
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				User user = new User();
				user.setEmail(rs.getString("email"));
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				user.setUserId(rs.getInt("user_id"));
				user.setUsername(rs.getString("user_name"));
				
				Role role = new Role();
				role.setRole(rs.getString("role_name"));
				role.setRoleId(rs.getInt("role_id"));
				user.setRole(role);
				
				users.add(user);
				
			}
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	@Override
	public User getUserById(Integer id) {
		
		try (Connection conn = cu.getConnection()){
			String sql = "select user_id, user_name, pass_word, first_name, last_name, email, role.role_id, role_name from "
					+ "bank_user join role on bank_user.role_id = role.role_id where user_id = ?";
			
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			
			ResultSet rs = pst.executeQuery();
			
			if(rs.next()) {
				return makeUser(rs);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public User updateUser(User p) {
		
		try(Connection conn = cu.getConnection()){
			
			String sql = "update bank_user set user_name = ?, pass_word = ?, first_name = ?, last_name = ?, email = ?,"
					+ " role_id = ? where user_id = ?";
			
			PreparedStatement pst = conn.prepareStatement(sql);
			
			pst.setString(1, p.getUsername());
			pst.setString(2, p.getPassword());
			pst.setString(3, p.getFirstName());
			pst.setString(4, p.getLastName());
			pst.setString(5, p.getEmail());
			pst.setInt(6, p.getRole().getRoleId());
			pst.setInt(7, p.getUserId());
			
			int rs = pst.executeUpdate();
			
			if(rs > 0) {
				return p;
			} 
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public User registerUser(User p) {

		try(Connection conn = cu.getConnection()){
			String sql = "insert into bank_user values(default, ?, ?, ?, ?, ?, ?)";
			String[] keys = {"user_id"};
			PreparedStatement pst = conn.prepareStatement(sql,keys);
			
			
			pst.setString(1, p.getUsername());
			pst.setString(2, p.getPassword());
			pst.setString(3, p.getFirstName());
			pst.setString(4, p.getLastName());
			pst.setString(5, p.getEmail());
			pst.setInt(6, p.getRole().getRoleId());
			pst.executeUpdate();
			ResultSet rs = pst.getGeneratedKeys();
			
			if(rs.next()) {
				p.setUserId(rs.getInt(1)); 
				return p;
			}
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public User getUserByUsername(String username) {

		try(Connection conn = cu.getConnection()){
			String sql = "select * from bank_user join role on bank_user.role_id = role.role_id where user_name = ?";
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(1, username);
			ResultSet rs = pst.executeQuery();
			
			
			if(rs.next()) {
				return makeUser(rs);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User getUserByAccount(Integer accountID) {
		User user = new User();
		
		try(Connection conn = cu.getConnection()){
			String sql = "select * from bank_user bu join user_account ua "
					+ "on bu.user_id = ua.user_id join role r on r.role_id = bu.role_id "
					+ "where ua.account_id =?";
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setInt(1, accountID);
			ResultSet rs = pst.executeQuery();
			if(rs.next()) {
				user = makeUser(rs);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}
	
	public User makeUser(ResultSet rs) {
		User user = new User();
		try {
		user.setEmail(rs.getString("email"));
		user.setFirstName(rs.getString("first_name"));
		user.setLastName(rs.getString("last_name"));
		user.setPassword(rs.getString("pass_word"));
		
		Role role = new Role();
		role.setRoleId(rs.getInt("role_id"));
		role.setRole(rs.getString("role_name"));
		user.setRole(role);
		
		user.setUserId(rs.getInt("user_id"));
		user.setUsername(rs.getString("user_name"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
		
	}

	@Override
	public boolean deleteUser(Integer userId) {
		
		try (Connection conn = cu.getConnection()){
			conn.setAutoCommit(false);
			Set<Integer> accountIds = new HashSet<>();
			String sql = "select ac.account_id from account ac join user_account ua on ac.account_id = ua.account_id where ua.user_id =?";
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setInt(1, userId);
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				accountIds.add(rs.getInt("account_id"));
			}
			sql = "delete from user_account where user_id = ?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, userId);
			
			Integer rs2 = pst.executeUpdate();
			
			//delete from multiplicity table was successful
			if(rs2 == accountIds.size()) {
				sql = "delete from bank_user where user_id = ?";
				pst = conn.prepareStatement(sql);
				pst.setInt(1, userId);
				
				rs2 = pst.executeUpdate();
				
				//delete from user table was successful 
				if(rs2 == 1) {
					//if the users have accounts to delete
					if(!accountIds.isEmpty()) {
						for(Integer id2: accountIds) {
							sql = "delete from account where account_id = ?";
							pst = conn.prepareStatement(sql);
							pst.setInt(1, id2);
							rs2 = pst.executeUpdate();
							//delete account was not successful
							if(!rs2.equals(1)) {
								conn.rollback();
								return false;
							}
						}
						//If none of the delete accounts attempts fails, everything was a success
						conn.commit();
						return true;
					//There were no accounts to delete, so everything was a success
					} else {
						conn.commit();
						return true;
					}
				//user record was not successfully deleted
				} else {
					conn.rollback();
					return false;
				}
			//entries in multiplicity table were not successfully deleted
			} else {
				conn.rollback();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean upgradeUser(Integer id, User user, Integer roleId) {
		
		try(Connection conn = cu.getConnection()){
			conn.setAutoCommit(false);
			String sql = "update bank_user set role_id = ? where user_id = ?";
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setInt(1, roleId);
			pst.setInt(2, user.getUserId());
			Integer rs = pst.executeUpdate();
			if (rs ==1) {
				sql = "update account set balance = (select balance from account where account_id=?) - 50 where account_id =?";
				pst = conn.prepareStatement(sql);
				pst.setInt(1, id);
				pst.setInt(2, id);
				rs = pst.executeUpdate();
				if(rs == 1) {
					conn.commit();
					return true;
				} else {
					conn.rollback();
					return false;
				}
			} else {
				conn.rollback();
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	
	
}
