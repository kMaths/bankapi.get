package dev.iafrate.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import dev.iafrate.beans.Role;
import dev.iafrate.util.ConnectionUtil;

public class RolePostgres implements RoleDAO {
	private ConnectionUtil cu = ConnectionUtil.getConnectionUtil();

	@Override
	public Set<Role> getAllRoles() {
		
		Set<Role> roles = new HashSet<>();
		
		try(Connection conn = cu.getConnection()){
			
			String sql = "select * from role";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Role role = new Role();
				role.setRole(rs.getString("role_name"));
				role.setRoleId(rs.getInt("role_id"));
				roles.add(role);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return roles;
	}

	@Override
	public Integer getRoleByName(String roleName) {

		try(Connection conn = cu.getConnection()){
			String sql = "Select role_id from role where role_name = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, roleName);
			ResultSet rs = ps.executeQuery();
			 if(rs.next()) {
				 return rs.getInt("role_id");
			 }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
}
