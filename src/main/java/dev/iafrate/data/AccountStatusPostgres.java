package dev.iafrate.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


import dev.iafrate.beans.AccountStatus;
import dev.iafrate.util.ConnectionUtil;

public class AccountStatusPostgres implements AccountStatusDAO {
	ConnectionUtil cu = ConnectionUtil.getConnectionUtil();
	
	@Override
	public AccountStatus getAccountStatusById(Integer id) {
		
		try(Connection conn = cu.getConnection()){
			String sql = "select * from account_status where account_status.status_id = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				AccountStatus status = new AccountStatus();
				status.setStatusId(rs.getInt("status_id"));
				status.setStatus(rs.getString("status_name"));
				return status;
			}	
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public AccountStatus getAccountStatusByName(String name) {
		
		try(Connection conn = cu.getConnection()){
			String sql = "select * from account_status where account_status.status_name = ?";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setString(1, name);
			
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				AccountStatus status = new AccountStatus();
				status.setStatusId(rs.getInt("status_id"));
				status.setStatus(rs.getString("status_name"));
				return status;
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<AccountStatus> getAllAccountStatus() {
		List<AccountStatus> statuses = new ArrayList<>();
		
		try(Connection conn = cu.getConnection()){
			
			String sql = "Select * from account_status";
			
			PreparedStatement pst = conn.prepareStatement(sql);
			ResultSet rs = pst.executeQuery();
			
			while (rs.next()) {
				AccountStatus status = new AccountStatus();
				status.setStatus(rs.getString("status_name"));
				status.setStatusId(rs.getInt("status_id"));;
				statuses.add(status);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return statuses;
	}

	

}
