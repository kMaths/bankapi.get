package dev.iafrate.data;

import java.util.List;
import dev.iafrate.beans.AccountStatus;

public interface AccountStatusDAO {
	
	public AccountStatus getAccountStatusById(Integer id);
	public AccountStatus getAccountStatusByName(String name);
	public List<AccountStatus> getAllAccountStatus();

}
