package dev.iafrate.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;
import dev.iafrate.beans.AccountType;
import dev.iafrate.util.ConnectionUtil;

public class AccountTypePostgres implements AccountTypeDAO {
	
	ConnectionUtil cu = ConnectionUtil.getConnectionUtil();
	
	@Override
	public Set<AccountType> getAllAccountTypes() {
		
		Set<AccountType> types = new HashSet<>();
		
		try(Connection conn = cu.getConnection()){
			String sql = "select * from account_type";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				AccountType type = new AccountType();
				type.setType(rs.getString("type_name"));
				type.setTypeId(rs.getInt("type_id"));
				types.add(type);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return types;
	}

	@Override
	public Integer getAccountIdByName(String accountName) {
		
		try(Connection conn = cu.getConnection()){
			
			String sql = "select type_id from account_type where account_type.type_name = ?";
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setString(1, accountName);
			ResultSet rs = pst.executeQuery();
			if(rs.next()) {
				return rs.getInt("type_id");
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
