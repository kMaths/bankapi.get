package dev.iafrate.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import dev.iafrate.beans.Account;
import dev.iafrate.beans.AccountStatus;
import dev.iafrate.beans.AccountType;
import dev.iafrate.beans.User;
import dev.iafrate.util.ConnectionUtil;

public class AccountPostgres implements AccountDAO {
	private ConnectionUtil cu = ConnectionUtil.getConnectionUtil();

	@Override
	public Set<Account> getAllAccounts() {
		Set<Account> accounts = new HashSet<>();

		try (Connection conn = cu.getConnection()) {
			String sql = "select account_id, balance, account.status_id, status_name, account.type_id, type_name from account "
					+ "join account_status on account.status_id = account_status.status_id "
					+ "join account_type on account.type_id = account_type.type_id;";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			accounts = makeAccount(rs);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return accounts;
	}

	@Override
	public Set<Account> getAccountsByUser(Integer id) {
		Set<Account> accounts = new HashSet<>();
		
		try	(Connection conn = cu.getConnection()){
			String sql = "select account.account_id, balance, account.status_id,"
					+ " status_name, account.type_id, type_name from account join account_status on"
					+ " account.status_id = account_status.status_id join account_type on account.type_id "
					+ "= account_type.type_id join user_account on user_account.account_id = "
					+ "account.account_id where user_account.user_id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			
			accounts = makeAccount(rs);
		
		} catch (Exception e) {
			e.printStackTrace();
		}

		return accounts;
	}

	@Override
	public Account getAccountById(Integer account_id) {
		
		
		try(Connection conn = cu.getConnection()){
			Account account = new Account();
			String sql = "select account_id, balance, account.status_id, status_name, account.type_id, "
					+ "type_name from account join account_status on account.status_id = account_status.status_id "
					+ "join account_type on account.type_id = account_type.type_id where account.account_id = ?";
			
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, account_id);
			
			ResultSet rs = pstmt.executeQuery();
			if(rs!=null) {
			account = makeAccount(rs).iterator().next();
			return account;
			} else {
				return null;
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Set<Account> getAccountByAccountStatus(Integer id) {
		Set<Account> accounts = new HashSet<>();
		
		try (Connection conn = cu.getConnection()) {
			String sql = "select account_id, balance, account.status_id, status_name, account.type_id,"
					+ " type_name from account join account_status on account.status_id = account_status.status_id "
					+ "join account_type on account.type_id = account_type.type_id where account.status_id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, id);
			
			ResultSet rs = pstmt.executeQuery();
			
			accounts = makeAccount(rs);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return accounts;
	}

	@Override
	public Account createAccount(Account account, User user) {
		try (Connection conn = cu.getConnection()) {
			conn.setAutoCommit(false);
			String sql = "INSERT INTO account values(default,?,?,?)";
			String[] keys = { "account_id" };
			PreparedStatement pst = conn.prepareStatement(sql, keys);
			pst.setDouble(1, account.getBalance());
			pst.setInt(2, account.getStatus().getStatusId());
			pst.setInt(3, account.getType().getTypeId());
			pst.executeUpdate();
			ResultSet rs = pst.getGeneratedKeys();

			if (rs.next()) {
				Integer accountId = rs.getInt(1);
				sql = "INSERT INTO user_account VALUES(?,?)";
				pst = conn.prepareStatement(sql);
				pst.setInt(1, accountId);
				pst.setInt(2, user.getUserId());
				if (pst.executeUpdate() > 0) {
					account.setAccountId(accountId);
					conn.commit();
					return account;
				} else {
					conn.rollback();
				}

			} else {
				conn.rollback();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Account updateAccount(Account account) {
		try (Connection conn = cu.getConnection()) {
			conn.setAutoCommit(false);
			String sql = "update account set balance = ?, status_id = ?, type_id =? where account_id = ?";
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setDouble(1, account.getBalance());
			pst.setInt(2, account.getStatus().getStatusId());
			pst.setInt(3, account.getType().getTypeId());
			pst.setInt(4, account.getAccountId());
			
			int rowsEffected = pst.executeUpdate();

			if (rowsEffected == 1) {
					conn.commit();
					return account;
			}
			
			conn.rollback();
			return null;	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Set<Account> makeAccount(ResultSet rs) throws SQLException {
		Set<Account> accounts = new HashSet<>();
		while(rs.next()) {
		Account account = new Account();
		account.setAccountId(rs.getInt("account_id"));
		account.setBalance(rs.getDouble("balance"));
		
		AccountStatus status = new AccountStatus();
		status.setStatus(rs.getString("status_name"));
		status.setStatusId(rs.getInt("status_id"));
		account.setStatus(status);
		
		AccountType type = new AccountType();
		type.setType(rs.getString("type_name"));
		type.setTypeId(rs.getInt("type_id"));
		account.setType(type);	
		
		accounts.add(account);
		}
		return accounts;
	}

	@Override
	public boolean addLink(Integer accountId, Integer userId) {
		try (Connection conn = cu.getConnection()){
			String sql = "insert into user_account values (?,?)";
			PreparedStatement pst = conn.prepareStatement(sql);
			pst.setInt(1, accountId);
			pst.setInt(2, userId);
			
			int rs = pst.executeUpdate();
			if(rs == 1) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
