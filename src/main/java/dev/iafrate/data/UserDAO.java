package dev.iafrate.data;

import java.util.Set;

import dev.iafrate.beans.User;

//CreateReadUpdateDelete
public interface UserDAO {
	public Set<User> getAllUsers();
	public User getUserById(Integer id);// Read
	public User updateUser(User p);
	public User registerUser(User p);// Create
	public User getUserByUsername(String username); 
	public User getUserByAccount(Integer accountId);
	public boolean deleteUser(Integer id);
	public boolean upgradeUser(Integer id, User user, Integer roleId);
	
}
