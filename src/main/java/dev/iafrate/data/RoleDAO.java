package dev.iafrate.data;

import java.util.Set;

import dev.iafrate.beans.Role;

public interface RoleDAO {
	public Set<Role> getAllRoles();
	public Integer getRoleByName(String roleName);
	
}
