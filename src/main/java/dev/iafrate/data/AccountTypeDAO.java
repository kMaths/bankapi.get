package dev.iafrate.data;

import java.util.Set;
import dev.iafrate.beans.AccountType;


public interface AccountTypeDAO {

	public Set<AccountType> getAllAccountTypes();
	public Integer getAccountIdByName(String accountName);

}
