package dev.iafrate.data;

import java.util.Set;

import dev.iafrate.beans.Account;
import dev.iafrate.beans.User;

public interface AccountDAO {
	Set<Account> getAllAccounts(); // read
	Set<Account> getAccountsByUser(Integer id);// read
	Account getAccountById(Integer account_id);// read
	Set<Account> getAccountByAccountStatus(Integer id); // read
	Account createAccount(Account account, User user); // create
	Account updateAccount(Account account); // update
	//For Premium Users
	boolean addLink(Integer accountId, Integer userId);
	
}
