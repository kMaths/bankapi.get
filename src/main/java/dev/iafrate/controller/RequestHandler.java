package dev.iafrate.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dev.iafrate.beans.Role;
import dev.iafrate.beans.User;
import dev.iafrate.delegates.AccountsDelegate;
import dev.iafrate.delegates.AdminDelegate;
import dev.iafrate.delegates.EmployeeDelegate;
import dev.iafrate.delegates.FrontControllerDelegate;
import dev.iafrate.delegates.LoginDelegate;
import dev.iafrate.delegates.LogoutDelegate;
import dev.iafrate.delegates.PremiumDelegate;
import dev.iafrate.delegates.RegisterDelegate;
import dev.iafrate.delegates.StandardDelegate;



public class RequestHandler {
	private Map<String, FrontControllerDelegate> delegateMap;
	
	{
		delegateMap = new HashMap<String, FrontControllerDelegate>();
		
		delegateMap.put("login", new LoginDelegate());
		delegateMap.put("logout", new LogoutDelegate());
		delegateMap.put("register", new RegisterDelegate());
		delegateMap.put("accounts", new AccountsDelegate());
		delegateMap.put("admin", new AdminDelegate());
		delegateMap.put("employee", new EmployeeDelegate());
		delegateMap.put("standard", new StandardDelegate());
		delegateMap.put("premium", new PremiumDelegate());
	}
	
	public FrontControllerDelegate handle(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// if the request is an OPTIONS request,
		// we will return an empty delegate
		if ("OPTIONS".equals(req.getMethod())) {
			return (r1, r2) -> {};
		}
		
		// first, we get the URI from the request
		StringBuilder uriString = new StringBuilder(req.getRequestURI());
		// at this point, uriString = localhost:8080/CatApp/cat/4
		
		// next, we get rid of the first part of the URL
		uriString.replace(0, req.getContextPath().length()+1, "");
		// at this point, uriString = user/login
		if (uriString.indexOf("/") != -1) {
			req.setAttribute("path", uriString.substring(uriString.indexOf("/")+1));
			uriString.replace(uriString.indexOf("/"), uriString.length(), "");
			// at this point, uriString = user
		}
		if(!uriString.toString().equals("login") && !uriString.toString().equals("logout")) {
			
			if(req.getSession().getAttribute("user")!=null) {
		
				if(uriString.toString().equals("users")) {
					User user = (User) req.getSession().getAttribute("user");
					Role role = user.getRole();
					return delegateMap.get(role.getRole());
				}
				return delegateMap.get(uriString.toString());
			} else {
				resp.sendError(401,"The requested action is not permitted");
				return (r1, r2) -> {};
			}
		}
		
		return delegateMap.get(uriString.toString());
	}
}
