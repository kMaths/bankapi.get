package dev.iafrate.services;

import java.util.Set;



//import java.util.Set;

import dev.iafrate.beans.Account;
import dev.iafrate.beans.AccountStatus;
//import dev.iafrate.beans.AccountStatus;
import dev.iafrate.beans.User;
import dev.iafrate.data.AccountDAO;
import dev.iafrate.data.AccountStatusDAO;
import dev.iafrate.data.UserDAO;

public class AccountService {

	AccountDAO accountDAO;
	UserDAO userDAO;
	AccountStatusDAO accountStatusDAO;

	public AccountService(AccountDAO accountDAO, UserDAO userDAO, AccountStatusDAO accountStatusDAO) {
		super();
		this.accountDAO = accountDAO;
		this.userDAO = userDAO;
		this.accountStatusDAO = accountStatusDAO;
		
	}

	public Account updateAccount(Account account, User user) {
		if("admin".equals(user.getRole().getRole())) {
			return accountDAO.updateAccount(account);
		}
		return null;
	}
	
	public Set<Account> getAccountsByStatus(Integer id){
		return accountDAO.getAccountByAccountStatus(id);
	}
	
	public Account submitAccount(Account account, User user) {
		return accountDAO.createAccount(account, user);
	}
	
	public Set<Account> getAllAccounts(){
		return accountDAO.getAllAccounts();
	}
	public Account getAccountById(Integer id, User user) {
		
		Account account = accountDAO.getAccountById(id);
		User user2 = userDAO.getUserByAccount(id);
		String roleName = user.getRole().getRole();
		if("admin".equals(roleName)|| "employee".equals(roleName) || user2.equals(user)) {
			return account;
		}
		return null;
	}
	public boolean exists(Account account) {
		Account account2 = accountDAO.getAccountById(account.getAccountId());
		if(account2.getAccountId()==0) {
			return false;
		} else {
			return true;
		}
	}
	public Set<Account> getAccountsByUser(Integer id, User user){
		Set<Account> accounts = accountDAO.getAccountsByUser(id);
		String role = user.getRole().getRole();
		
		if( "admin".equals(role) || "employee".equals(role) || user.getUserId()==id) {
			return accounts;
		} else {
			return null;
		}
	}
	public boolean transferFunds(User user, Integer sourceId, Integer targetId, Double amount) {
		Account sourceAccount = accountDAO.getAccountById(sourceId);
		Account targetAccount = accountDAO.getAccountById(targetId);
		User sourceOwner = userDAO.getUserByAccount(sourceId);
		String role = user.getRole().getRole();
		if(sourceAccount != null && targetAccount != null && ("admin".equals(role) || user.equals(sourceOwner))) {
			sourceAccount.setBalance(sourceAccount.getBalance()-amount);
			targetAccount.setBalance(targetAccount.getBalance()+amount);
			accountDAO.updateAccount(sourceAccount);
			accountDAO.updateAccount(targetAccount);
			return true;
		} else {
		return false;
		}
	}
	public boolean deleteAccount(Integer id) {
		Account account = accountDAO.getAccountById(id);
		AccountStatus status = accountStatusDAO.getAccountStatusByName("closed");
		account.setStatus(status);
		account = accountDAO.updateAccount(account);
		if(account!=null && account.getAccountId()!=0 && account.getStatus().equals(status)) {
			return true;
		}
		return false;
	
	}
	
	public boolean addUser(Integer userId, Integer accountId){
		User user = userDAO.getUserById(userId);
		if("premium".equals(user.getRole().getRole()) ) {
			return accountDAO.addLink(accountId, userId);
		}
		return false;
	}

}
