package dev.iafrate.services;

import java.util.Set;

import dev.iafrate.beans.Account;
import dev.iafrate.beans.User;
import dev.iafrate.data.AccountDAO;
import dev.iafrate.data.RoleDAO;
import dev.iafrate.data.UserDAO;

public class UserService {
	
	private UserDAO userDAO;
	private RoleDAO roleDAO;
	private AccountDAO accountDAO;

	public UserService(UserDAO userDAO, RoleDAO roleDAO, AccountDAO accountDAO) {
		this.userDAO = userDAO;
		this.roleDAO = roleDAO;
		this.accountDAO = accountDAO;
	}
	public User updateUser(User user) {
		return userDAO.updateUser(user);
	}
	
	public User getUserByUsername(String username) {
		return userDAO.getUserByUsername(username);
	}
	
	public Integer registerUser(User user) {
		return userDAO.registerUser(user).getUserId();
	}
	public Set<User> getAllUsers(){
		return userDAO.getAllUsers();
	}
	public User getUserById(Integer id) {
		return userDAO.getUserById(id);
	}
	public boolean userExists(User user) {
		return (userDAO.getUserById(user.getUserId())!=null);
	}
	public boolean deleteUser(Integer id) {
		return userDAO.deleteUser(id);
	}
	public boolean upgradeUser(Integer id, User user) {
		Integer roleId = roleDAO.getRoleByName("premium");
		Set<Account> accounts = accountDAO.getAccountsByUser(user.getUserId());
		for(Account account : accounts) {
			if(account.getAccountId()==id) {
				return userDAO.upgradeUser(id, user, roleId);
			}
		}
		return false;
		
	}
	

}
