insert into role values
	(default, 'admin'),
	(default, 'employee'),
	(default, 'standard'),
	(default, 'premium');
	
insert into account_status values
	(default, 'pending'),
	(default, 'open'),
	(default, 'closed'),
	(default, 'denied');
	
insert into account_type values
	(default, 'checking'),
	(default, 'saving');