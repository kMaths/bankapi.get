--get all accounts
select account_id, balance, account.status_id, status_name, account.type_id, type_name from account join account_status on account.status_id = account_status.status_id join account_type on account.type_id = account_type.type_id;

-- get account by account id
select account_id, balance, account.status_id, status_name, account.type_id, type_name from account join account_status on account.status_id = account_status.status_id join account_type on account.type_id = account_type.type_id where account.account_id =2;

--get accounts by status
select account_id, balance, account.status_id, status_name, account.type_id, type_name from account join account_status on account.status_id = account_status.status_id join account_type on account.type_id = account_type.type_id where account.status_id =2;

--populate user table and user_account table
	--insert into bank_user values (default, 'kiafrate', 'password', 'kelsey', 'iafrate', 'kiafrate13@gmail.com', 1);
	--insert into user_account values (1,1);

--get acounts by user
select account.account_id, balance, account.status_id, status_name, account.type_id, type_name from account join account_status on account.status_id = account_status.status_id join account_type on account.type_id = account_type.type_id join user_account on user_account.account_id = account.account_id where user_account.user_id = 1;


--update account
update account set balance = 450, status_id = 2, type_id =1 where account_id = 1;
select * from account;