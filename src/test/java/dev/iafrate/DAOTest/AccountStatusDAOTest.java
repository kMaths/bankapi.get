package dev.iafrate.DAOTest;

import java.util.List;

import dev.iafrate.beans.AccountStatus;
import dev.iafrate.data.AccountStatusDAO;
import dev.iafrate.data.AccountStatusPostgres;

public class AccountStatusDAOTest {

	public static void main(String[] args) {
		AccountStatusDAO accountDao = new AccountStatusPostgres();
		
		//Get id by name
		System.out.println(accountDao.getAccountStatusById(1));
		System.out.println();
		
		//Get all statuses
		List<AccountStatus> statuses = accountDao.getAllAccountStatus();
		for(AccountStatus status : statuses) {
			System.out.println(status);
		}
	}

}
