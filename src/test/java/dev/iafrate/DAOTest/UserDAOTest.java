package dev.iafrate.DAOTest;

import java.util.Set;

import dev.iafrate.beans.Role;
import dev.iafrate.beans.User;
import dev.iafrate.data.UserDAO;
import dev.iafrate.data.UserPostgres;

public class UserDAOTest {

	public static void main(String[] args) {
		UserDAO userDAO = new UserPostgres();
		
		//Get all users
		System.out.println("Get all users");
		Set<User> users = userDAO.getAllUsers();
		for(User user : users) {
			System.out.println(user);
		}
		System.out.println();
		
		//Get user by user_id
		System.out.println("Get user by user_id");
		System.out.println(userDAO.getUserById(1));
		System.out.println();
		
		//Update Person
		System.out.println("update user");
		User user = new User();
		user.setEmail("kiafrate13@gmail.com");
		user.setFirstName("Kelsey");
		user.setLastName("Iafrate");
		user.setPassword("password");
		Role role = new Role();
		role.setRoleId(1);
		role.setRole("admin");
		user.setRole(role);
		user.setUserId(1);
		user.setUsername("kiafrate");
		System.out.println(userDAO.updateUser(user));
		System.out.println();
		
		//Register user: commented out because we don't want to keep 
		//registering this user every time we run these tests
//		System.out.println("register user");
//		user.setUserId(0);
//		user.setUsername("Kelso");
//		System.out.println(user);
//		System.out.println(userDAO.registerUser(user));
		
		//Get User by Username
		System.out.println("Get user by username");
		System.out.println(userDAO.getUserByUsername("Kelso"));
		
		//Get User by Account id
		System.out.println("Get user by account id");
		System.out.println(userDAO.getUserByAccount(1));
		
		//Delete User
		System.out.println("Delete User");
		userDAO.deleteUser(6);
		
		
		
		
		
		
	}

}
