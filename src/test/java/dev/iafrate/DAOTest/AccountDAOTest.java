package dev.iafrate.DAOTest;

import java.util.Set;

import dev.iafrate.beans.Account;
import dev.iafrate.beans.AccountStatus;
import dev.iafrate.beans.AccountType;
import dev.iafrate.beans.Role;
import dev.iafrate.beans.User;
import dev.iafrate.data.AccountDAO;
import dev.iafrate.data.AccountPostgres;

public class AccountDAOTest {
	public static void main(String[] args) {
		
		AccountDAO accountDAO = new AccountPostgres();
		
		System.out.println("Get all Accounts");
		Set<Account> accounts = accountDAO.getAllAccounts();
		for(Account account : accounts) {
			System.out.println(account);
		}
		System.out.println();
		
		System.out.println("get accounts by user");
		User user = new User();
		user.setEmail("kaifrate13@gmail.com");
		user.setFirstName("kelsey");
		user.setLastName("iafrate");
		user.setPassword("password");
		Role admin = new Role();
		admin.setRole("admin");
		admin.setRoleId(1);
		user.setRole(admin);
		user.setUserId(1);
		user.setUsername("kiafrate");
		
		accounts = accountDAO.getAccountsByUser(1);
		for(Account account : accounts) {
			System.out.println(account);
		}
		System.out.println();
		
		System.out.println("Get account by Id");
		System.out.println(accountDAO.getAccountById(1));
		System.out.println();
		
		System.out.println("create Account");
		Account account2 = new Account();

		AccountStatus status = new AccountStatus();
		
		//set id to 2 here when testing update
		account2.setAccountId(2);
		status.setStatusId(2);
		status.setStatus("open");
		account2.setStatus(status);
		
		AccountType type = new AccountType();
		type.setType("checking");
		type.setTypeId(1);
		account2.setType(type);
		
		account2.setBalance(300.0);
		
//	We don't want to keep creating this account every time we test
//		System.out.println(accountDAO.createAccount(account2, user));
		System.out.println();
		System.out.println("Get Account By Status");
		accounts = accountDAO.getAccountByAccountStatus(1);
		for(Account account : accounts) {
			System.out.println(account);
		}
		System.out.println();
		
		System.out.println("Update Account");
		account2.setBalance(150);
		System.out.print(accountDAO.updateAccount(account2));
		
	
	}
}
