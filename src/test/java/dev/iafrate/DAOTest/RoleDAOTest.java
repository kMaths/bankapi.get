package dev.iafrate.DAOTest;

import java.util.Set;
import dev.iafrate.beans.Role;
import dev.iafrate.data.RoleDAO;
import dev.iafrate.data.RolePostgres;

public class RoleDAOTest {

	public static void main(String[] args) {
		RoleDAO roleDao = new RolePostgres();
		
		System.out.println("Get all roles");
		Set<Role> roles = roleDao.getAllRoles();
		for(Role role : roles) {
			System.out.println(role);
		}
		
		System.out.println();
		System.out.println("get role id by name");
		
		System.out.println(roleDao.getRoleByName("admin"));
	}

}
