package dev.iafrate.DAOTest;

import java.util.Set;

import dev.iafrate.beans.AccountType;
import dev.iafrate.data.AccountTypePostgres;

public class AccountTypeDAOTest {

	public static void main(String[] args) {
		AccountTypePostgres typeDAO = new AccountTypePostgres();
		
		//Get all types
		System.out.println("Get all types");
		Set<AccountType> types = typeDAO.getAllAccountTypes();
		for(AccountType type : types) {
			System.out.println(type);
		}
		
		System.out.println();
		System.out.println("Get type id by name");
		System.out.println(typeDAO.getAccountIdByName("saving"));

	}

}
