create table bank_user(
	user_id serial primary key,
	user_name varchar(30) unique not null,
	pass_word varchar (30) not null,
	first_name varchar (30)not null,
	last_name varchar (30) not null,
	email varchar (30) not null,
	role_id integer references role 
);

create table role(
	role_id serial primary key,
	role_name varchar(30) unique not null
);

create table account_type(
	type_id serial primary key,
	type_name varchar(30) unique not null
);

create table account_status(
	status_id serial primary key,
	status_name varchar(30) unique not null
);

create table account(
	account_id serial primary key,
	balance double precision not null,
	status_id integer references account_status,
	type_id integer references account_type
)

create table user_account(
	account_id integer references account,
	user_id integer references bank_user,
	primary key(account_id,user_id)
)

